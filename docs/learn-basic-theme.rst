Learn basic theme project
=========================

Second non-theme here is learn_basic_theme_project that is helpful for learning to develop new themes. It shows used templates, sidebars, and blocks of Sphinx builtin basic theme.

See `learn_basic_theme_project in action </learn_basic_theme_project>`_.

.. image:: ../learn_basic_theme_project/screenshot.png
