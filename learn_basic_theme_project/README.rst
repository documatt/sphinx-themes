#########################
Learn basic theme project
#########################

Is Sphinx project with "learn_basic" theme  that is identical to Sphinx builtin basic theme, only annotates templates, sidebars, and blocks with <div>s. Additional stylesheet makes these annotations better visible. It is useful if you are learning Sphinx theming.

This folder contains sources, see `learn_basic_theme_project in action <https://documatt.gitlab.io/sphinx-themes/learn_basic_theme_project>`_.

.. image:: screenshot.png
