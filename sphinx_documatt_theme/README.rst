#####################
Documatt Sphinx Theme
#####################

Mobile friendly Sphinx theme designed to provide great documentation reading experience with beautiful typography.

This theme is default theme of Documatt's `Techwriter at work blog <https://techwriter.documatt.com>`_ but you are welcome to use it with any Sphinx project.

.. rubric:: Documentation https://documatt.gitlab.io/sphinx-themes/themes/documatt.html

.. absolute image URL because it will be embedded also to PyPI

.. image:: https://gitlab.com/documatt/sphinx-themes/-/raw/master/sphinx_documatt_theme/screenshot.png?inline=false

*********
Impressum
*********

Project is open source licensed under BSD3.

External link icon by `icons8 <https://icons8.com>`_.
