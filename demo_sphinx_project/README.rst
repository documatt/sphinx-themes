###################
Demo Sphinx Project
###################

This Sphinx project shows almost all possible reStructuredText syntax markup. It is heavily based on original `Docutils demo.txt <https://repo.or.cz/docutils.git/blob_plain/HEAD:/docutils/docs/user/rst/demo.txt>`_, but with Sphinx additions.

It is useful if you are crafting a theme and wanna see show if don't forgot to style some part of Sphinx generated markup.

For example, see `demo_sphinx_project in action <https://documatt.gitlab.io/sphinx-themes/sphinx_documatt_theme>`_ with `sphinx_documatt_theme <https://gitlab.com/documatt/sphinx-themes/-/tree/master/sphinx_documatt_theme>`_.
