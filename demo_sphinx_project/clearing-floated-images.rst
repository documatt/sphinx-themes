.. image:: img/logo.svg
   :align: right

##################################
Clearing images before new section
##################################

The particular tricky scenario is floated image followed by the structure body element like section, rubric, admonition, transition, or the topic. The structure element must clear the floating (``clear: both`` in CSS).

.. image:: img/clear_floated_image.png

.. image:: img/logo.svg
   :align: left

**********
Section L2
**********

.. image:: img/logo.svg
   :align: right

Section L3
==========

.. image:: img/logo.svg
   :align: left

Section L4
----------

.. image:: img/logo.svg
   :align: right

Section L5
^^^^^^^^^^

.. image:: img/logo.svg
   :align: left

Section L6
''''''''''

.. image:: img/logo.svg
   :align: right

**********
Section L2
**********

.. image:: img/logo.svg
   :align: left

Section L3
==========

.. image:: img/logo.svg
   :align: right

Section L4
----------

.. image:: img/logo.svg
   :align: left

Section L5
^^^^^^^^^^

.. image:: img/logo.svg
   :align: right

Section L6
''''''''''

.. image:: img/logo.svg
   :align: left

.. rubric:: Rubric after left floated image

.. image:: img/logo.svg
   :align: right

.. rubric:: Rubric after right floated image

.. image:: img/logo.svg
   :align: left

.. caution:: Caution before floated image

.. image:: img/logo.svg
   :align: right

.. danger:: Danger before floated image

.. image:: img/logo.svg
   :align: left

.. error:: Error before floated image

.. image:: img/logo.svg
   :align: right

.. hint:: Hint before floated image

.. image:: img/logo.svg
   :align: left

.. important:: Important before floated image

.. image:: img/logo.svg
   :align: right

.. note:: Note before floated image

.. image:: img/logo.svg
   :align: left

.. tip:: Tip before floated image

.. image:: img/logo.svg
   :align: right

.. warning:: Warning before floated image

.. image:: img/logo.svg
   :align: left

.. admonition:: Generic admonition before floated image

   Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.

.. image:: img/logo.svg
   :align: right

----------------------------
