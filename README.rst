.. |project| replace:: Documatt Sphinx Themes Collection

#########
|project|
#########

.. image:: icons8-change-theme-64.png
   :align: right

Documatt Sphinx Themes is continuously growing collection of themes for Sphinx documentation projects. Our aim is to provide high quality themes suitable both for documentation and prose. Themes are primary targeted to `Documatt.com <https://documatt.com>`_ users, but anyone is welcome to use it with any Sphinx project.

* Documentation and available themes at https://documatt.gitlab.io/sphinx-themes
* The project is hosted at GitLab URL https://gitlab.com/documatt/sphinx-themes/, where are issues and contributions welcomed too.

*********
Impressum
*********

|project| is open source licensed under BSD3.

Project `Change Theme icon <https://icons8.com/icons/set/change-theme>`_ by `icons8 <https://icons8.com>`_.
